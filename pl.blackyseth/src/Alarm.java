import java.util.ArrayList;
import java.util.List;

public class Alarm {

    private List<AlarmListener> alarmListeners;
    private String pin;

    public Alarm(){
        this.alarmListeners = new ArrayList<AlarmListener>();
        thread.start();
    }

    Thread thread = new Thread() {
        public void run() {
            while(true) {
                int random = (int) (Math.random()*2);
                switch (random){
                    case 0: {pin="1234";break;}
                    case 1: {pin="1111";break;}
                }
                enterPin(pin);
                try {
                    this.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public void addListener(AlarmListener alarmListener){
        alarmListeners.add(alarmListener);
    }

    public void removeListener(AlarmListener alarmListener){
        this.alarmListeners.remove(alarmListener);
    }

    public void enterPin(String pin){
        if (pin.equals("1234")){
            correctEnteredPin();
        } else {
            wrongEnteredPin();
        }
    }

    public void wrongEnteredPin(){
        EnteredPinEvent event = new EnteredPinEvent(this);
        for (AlarmListener al: alarmListeners){
            al.alarmTurnedOn(event);
        }
    }

    public void correctEnteredPin(){
        EnteredPinEvent event = new EnteredPinEvent(this);
        for (AlarmListener al: alarmListeners){
            al.alarmTurnedOff(event);
        }
    }

}
