import java.util.Date;

public class EnteredPinEvent {
    private Alarm alarm;
    private Date date;

    EnteredPinEvent(Alarm alarm){
        date = new Date();
        this.alarm = alarm;
        System.out.println("Alarm: " + date + " ");
    }

    public Alarm getAlaram(){
        return alarm;
    }
}
