
public class Main {
    public static void main(String args[]){
        SoundAlert soundAlert = new SoundAlert();
        Bars bars = new Bars();
        Dogs dogs = new Dogs();
        Police police = new Police();

        Alarm alarmik = new Alarm();
        alarmik.addListener(bars);
        alarmik.addListener(soundAlert);
        alarmik.addListener(dogs);
        alarmik.addListener(police);

    }
}
